package reader.services.app

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import reader.services.app.databinding.ActivityNextButtonBinding

class NextButton : Activity() {
    lateinit var binding: ActivityNextButtonBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNextButtonBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val btn404 = binding.btn404
        btn404.setOnClickListener() {
            val ventana1: Intent = Intent(applicationContext, Error404::class.java);
            startActivity(ventana1)
        }

        val btn200 = binding.btn200
        btn200.setOnClickListener(){
            val ventana2: Intent = Intent(applicationContext, Status200::class.java);
            startActivity(ventana2)
        }
        val btn500 = binding.btn500
        btn500.setOnClickListener(){
            val ventan3: Intent = Intent(applicationContext, Error500::class.java);
            startActivity(ventan3)
        }
    }
}

package reader.services.app

import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.androidnetworking.interfaces.StringRequestListener
import reader.services.app.databinding.ActivityMainBinding
import com.newrelic.agent.android.NewRelic;

class MainActivity : Activity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        NewRelic.withApplicationToken(
            "Aqui va su token"
        ).start(this.getApplicationContext());

        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnLoadUrl.setOnClickListener { loadService() }
        binding.btnCopyResponse.setOnClickListener { copyResponse() }

        val btnNext = binding.btnNext
        btnNext.setOnClickListener() {
            val ventana2: Intent = Intent(applicationContext, NextButton::class.java);
            startActivity(ventana2)
        }
    }

    fun loadService() {
        if (binding.edtUrl.text.toString().equals("")) return

        binding.vLoading.visibility = View.VISIBLE
        binding.txtResponseBody.setText("")

        AndroidNetworking.post(binding.edtUrl.text.toString())
            .setPriority(Priority.IMMEDIATE)
            .build()
            .getAsString(object : StringRequestListener {

                override fun onResponse(response: String?) {
                    binding.txtResponseBody.setText(response)
                    binding.vLoading.visibility = View.GONE
                }

                override fun onError(anError: ANError?) {
                    binding.txtResponseBody.setText(anError.toString())
                    binding.vLoading.visibility = View.GONE
                }
            })
    }

    fun copyResponse() {
        val clipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clipData = ClipData.newPlainText("text", binding.txtResponseBody.text.toString())
        clipboardManager.setPrimaryClip(clipData)
    }


}
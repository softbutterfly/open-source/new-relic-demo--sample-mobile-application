package reader.services.app

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import reader.services.app.databinding.ActivityError500Binding

class Error500 : Activity() {
    lateinit var binding: ActivityError500Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityError500Binding.inflate(layoutInflater)
        setContentView(binding.root)

        val btnReturn = binding.btnReturn
        btnReturn.setOnClickListener() {
            val ventana: Intent = Intent(applicationContext, NextButton::class.java);
            startActivity(ventana)
        }
    }
}